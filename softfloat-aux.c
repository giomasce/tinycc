#ifdef USE_SOFTFLOAT

#include "softfloat-aux.h"

#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>
#include <assert.h>
#include <stdlib.h>

static float64_t num_0x1pxx(int xx) {
    float64_t ret;
    ret.v = (uint64_t)(1023 + xx) << 52;
    return ret;
}

// From musl/src/math/scalbn.c
float64_t f64_ldexp(float64_t x, int n) {
    union {float64_t f; uint64_t i;} u;
    float64_t y = x;

    if (n > 1023) {
        y = f64_mul(y, num_0x1pxx(1023));
        n -= 1023;
        if (n > 1023) {
            y = f64_mul(y, num_0x1pxx(1023));
            n -= 1023;
            if (n > 1023)
                n = 1023;
        }
    } else if (n < -1022) {
        /* make sure final n < -53 to avoid double
           rounding in the subnormal range */
        y = f64_mul(y, f64_mul(num_0x1pxx(-1022), num_0x1pxx(53)));
        n += 1022 - 53;
        if (n < -1022) {
            y = f64_mul(y, f64_mul(num_0x1pxx(-1022), num_0x1pxx(53)));
            n += 1022 - 53;
            if (n < -1022)
                n = -1022;
        }
    }
    u.i = (uint64_t)(0x3ff+n)<<52;
    x = f64_mul(y, u.f);
    return x;
}

float32_t strtosf32(const char *ptr, char **end) {
    float x = strtof(ptr, end);
    return *(float32_t*)&x;
}

float64_t strtosf64(const char *ptr, char **end) {
    double x = strtod(ptr, end);
    return *(float64_t*)&x;
}

extFloat80_t strtosExtF80(const char *ptr, char **end) {
    long double x = strtold(ptr, end);
    return *(extFloat80_t*)&x;
}

extFloat80_t extF80_negate(extFloat80_t x) {
    extFloat80_t y = x;
    y.signExp = y.signExp ^ 0x8000;
    return y;
    //return extF80_sub(extF80_mul(i32_to_extF80(-1), ui32_to_extF80(0)), x);
}

void extF80_store(extFloat80_t x, void *ptr) {
    *(uint64_t*)ptr = x.signif;
    *((uint16_t*)ptr+4) = x.signExp;
}

#endif
