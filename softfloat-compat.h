
typedef float float32_t;
typedef double float64_t;
typedef long double extFloat80_t;

#define i32_to_f32(x) ((float)(x))
#define ui32_to_f32(x) ((float)(x))

#define i32_to_f64(x) ((double)(x))
#define ui32_to_f64(x) ((double)(x))
#define i64_to_f64(x) ((double)(x))
#define ui64_to_f64(x) ((double)(x))

#define i32_to_extF80(x) ((long double)(x))
#define ui32_to_extF80(x) ((long double)(x))
#define i64_to_extF80(x) ((long double)(x))
#define ui64_to_extF80(x) ((long double)(x))
#define extF80_to_ui64_r_minMag(x,y) ((uint64_t)(x))

#define f64_to_f32(x) ((float)(x))
#define extF80_to_f32(x) ((float)(x))
#define extF80_to_f64(x) ((double)(x))
#define f32_to_extF80(x) ((long double)(x))
#define f64_to_extF80(x) ((long double)(x))

#define f32_add(x,y) (((float)(x))+((float)(y)))
#define f32_sub(x,y) (((float)(x))-((float)(y)))
#define f32_mul(x,y) (((float)(x))*((float)(y)))
#define f32_div(x,y) (((float)(x))/((float)(y)))

#define f64_add(x,y) (((double)(x))+((double)(y)))
#define f64_sub(x,y) (((double)(x))-((double)(y)))
#define f64_mul(x,y) (((double)(x))*((double)(y)))
#define f64_div(x,y) (((double)(x))/((double)(y)))

#define extF80_add(x,y) (((long double)(x))+((long double)(y)))
#define extF80_sub(x,y) (((long double)(x))-((long double)(y)))
#define extF80_mul(x,y) (((long double)(x))*((long double)(y)))
#define extF80_div(x,y) (((long double)(x))/((long double)(y)))

#define f64_ldexp(x,y) (ldexp((double)(x), y))

#define extF80_eq(x,y) (((long double)(x))==((long double)(y)))
#define extF80_negate(x) (-(long double)(x))

#define strtosf32(x,y) (strtof((x),(y)))
#define strtosf64(x,y) (strtod((x),(y)))
#define strtosExtF80(x,y) (strtold((x),(y)))

#define extF80_store(x,ptr) (*((long double*)(ptr))=(((long double)(x))))
