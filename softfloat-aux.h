#ifndef SOFTFLOAT_AUX_H
#define SOFTFLOAT_AUX_H

#include "softfloat.h"

_Static_assert(sizeof(float) == sizeof(float32_t), "wrong float size");
_Static_assert(sizeof(double) == sizeof(float64_t), "wrong double size");
_Static_assert(sizeof(long double) == sizeof(extFloat80_t), "wrong long double size");

float64_t f64_ldexp(float64_t x, int n);
float32_t strtosf32(const char *ptr, char **end);
float64_t strtosf64(const char *ptr, char **end);
extFloat80_t strtosExtF80(const char *ptr, char **end);
extFloat80_t extF80_negate(extFloat80_t x);
void extF80_store(extFloat80_t x, void *ptr);

#endif
